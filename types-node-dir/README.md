# Installation
> `npm install --save @types/node-dir`

# Summary
This package contains type definitions for node-dir (https://github.com/fshost/node-dir).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/node-dir.

### Additional Details
 * Last updated: Wed, 07 Jul 2021 00:01:48 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Panu Horsmalahti](https://github.com/panuhorsmalahti), and [James Lismore](https://github.com/jlismore).
